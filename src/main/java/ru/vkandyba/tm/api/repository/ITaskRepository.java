package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task findByName(String userId, String name);

    Task removeByName(String userId, String name);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    Task unbindTaskToProjectById(String userId, String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void clear(String userId);

}
