package ru.vkandyba.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IAuthService getAuthService();

    IUserService getUserService();

}
