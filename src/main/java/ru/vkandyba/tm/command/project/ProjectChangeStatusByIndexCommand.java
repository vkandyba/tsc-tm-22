package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change status project by index...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }


}
