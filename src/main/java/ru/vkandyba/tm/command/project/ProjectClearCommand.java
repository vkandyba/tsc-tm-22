package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove all projects...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
