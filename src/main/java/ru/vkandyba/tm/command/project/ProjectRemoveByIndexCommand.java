package ru.vkandyba.tm.command.project;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
