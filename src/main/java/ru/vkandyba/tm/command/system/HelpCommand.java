package ru.vkandyba.tm.command.system;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of commands...";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.name() + ": " + command.description());
        }
    }

}
