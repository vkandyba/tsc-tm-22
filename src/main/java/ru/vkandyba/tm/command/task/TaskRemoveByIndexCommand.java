package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
