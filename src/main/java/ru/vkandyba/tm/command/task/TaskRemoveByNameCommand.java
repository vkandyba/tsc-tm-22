package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().removeByName(userId, name);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
