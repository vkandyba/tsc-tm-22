package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskShowAllByProjectIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "show-all-tasks-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println(serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId));
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
