package ru.vkandyba.tm.command.user;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.util.TerminalUtil;

public class AuthChangePasswordCommand extends AbstractCommand {

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password...";
    }

    @Override
    public void execute() {
        if (!serviceLocator.getAuthService().isAuth()) throw new AccessDeniedException();
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(serviceLocator.getAuthService().getUserId(), password);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
