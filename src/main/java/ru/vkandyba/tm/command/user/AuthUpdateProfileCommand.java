package ru.vkandyba.tm.command.user;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.util.TerminalUtil;

public class AuthUpdateProfileCommand extends AbstractCommand {

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile...";
    }

    @Override
    public void execute() {
        if (!serviceLocator.getAuthService().isAuth()) {
            throw new AccessDeniedException();
        }
        System.out.println("Enter First Name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                serviceLocator.getAuthService().getUserId(),
                firstName, lastName, middleName
        );
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
