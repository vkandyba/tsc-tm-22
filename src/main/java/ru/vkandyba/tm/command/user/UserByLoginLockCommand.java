package ru.vkandyba.tm.command.user;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class UserByLoginLockCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "lock user by login...";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
