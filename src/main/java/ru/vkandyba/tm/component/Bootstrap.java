package ru.vkandyba.tm.component;

import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.command.project.*;
import ru.vkandyba.tm.command.system.*;
import ru.vkandyba.tm.command.task.*;
import ru.vkandyba.tm.command.user.*;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.system.UnknownCommandException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.*;
import ru.vkandyba.tm.service.*;

import java.util.Scanner;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectRemoveWithTasksByIdCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskShowAllByProjectIdCommand());
        registry(new TaskBindToProjectByIdCommand());
        registry(new TaskUnbindFromProjectByIdCommand());

        registry(new ViewInfoCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new UserByLoginLockCommand());
        registry(new UserByLoginUnlockCommand());
        registry(new UserByLoginRemoveCommand());
    }

    private void initData() {
        projectService.add(new Project("Project Q", "-")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Project B", "-"));
        projectService.add(new Project("Project F", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task A", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task V", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Task S", "-"));
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        initData();
        initUsers();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = scanner.nextLine();
                logService.command(command);
                execute(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }


    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void execute(String command) {
        if (command == null) return;
        final AbstractCommand abstractCommand = getCommandService().getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    public IUserService getUserService() {
        return userService;
    }
}
