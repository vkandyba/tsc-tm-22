package ru.vkandyba.tm.model;

public abstract class AbstractBusinessEntity extends AbstractEntity {

    protected String userId = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
