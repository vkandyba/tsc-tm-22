package ru.vkandyba.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String Id) {
        this.id = Id;
    }

}
