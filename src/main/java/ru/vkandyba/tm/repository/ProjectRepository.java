package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(String userId, String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project removeByName(String userId, String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElse(null);
    }

    @Override
    public Project startById(String userId, String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(String userId, String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(String userId, String id, Status status) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(String userId, Integer index, Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String userId, String name, Status status) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public void clear(String userId) {
        list.removeAll(findAll(userId));
    }

}
