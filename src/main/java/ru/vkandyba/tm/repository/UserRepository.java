package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeUser(User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        list.remove(user);
        return user;
    }

}
