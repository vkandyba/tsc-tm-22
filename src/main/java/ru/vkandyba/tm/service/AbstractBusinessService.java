package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessRepository<E> {

    protected IBusinessRepository<E> repository;

    public AbstractBusinessService(IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @Override
    public List findAll(String userId, Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (comparator == null) throw new RuntimeException();
        return repository.findAll(userId, comparator);
    }

    @Override
    public Boolean existsByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.existsByIndex(userId, index);
    }

    @Override
    public Boolean existsById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Override
    public E findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public E findByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @Override
    public E removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public E removeByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public E add(String userId, E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        return repository.add(userId, entity);
    }

    @Override
    public void remove(String userId, E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        repository.remove(userId, entity);
    }

}
