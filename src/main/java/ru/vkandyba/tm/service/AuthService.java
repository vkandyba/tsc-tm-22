package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.service.IAuthService;
import ru.vkandyba.tm.api.service.IUserService;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.empty.EmptyEmailException;
import ru.vkandyba.tm.exception.empty.EmptyLoginException;
import ru.vkandyba.tm.exception.empty.EmptyPasswordException;
import ru.vkandyba.tm.exception.entity.UserNotFoundException;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
