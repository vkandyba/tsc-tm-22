package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.api.service.ICommandService;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.model.Command;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getListCommandName();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getListCommandArg();
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }
}
